﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_6_delegates
{

    public delegate void MessengerDelegate(string message);

    public class Account
    {
        public event MessengerDelegate AccountHandler;

        private MessengerDelegate messenger;
        public string PersonFullName { get; set; }
        public int Sum { get; private set; } = 1;

        public void Add(int sum)
        {
            Sum += sum;
            AccountHandler?.Invoke("Вы внесли " + sum);

        }
        //или сокращенный вариант
        public void Withdraw(int sum)
        {
            if (sum > Sum)
            {
                AccountHandler?.Invoke("Сумма больше " + sum);
                return;
            }

            Sum -= sum;
            AccountHandler?.Invoke("Вы сняли " + sum);
        }

    }
}
