﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_6_delegates
{
    class Program
    {
        static void Main(string[] args)
        {
            Account account = new Account
            {
                PersonFullName = "Иванов Иван Иванович",
            };

            account.AccountHandler += message => { new ConsoleMessenger().SendMessage(message); };
            //account.AccountHandler += message => { Console.WriteLine(message); };

            account.Add(500);
            account.Withdraw(200);

            string[] cities = { "Астана", "Прага", "Пекин" };
            string[] citiesA = new string[3];
            int counter = 0;

            foreach (var city in cities)
            {
                if (city.ToLower().Contains("a"))
                {
                    citiesA[counter] = city;
                    counter++;
                }
            }


            Console.ReadLine();
        }
    }
}

