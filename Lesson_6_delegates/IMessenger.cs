﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_6_delegates
{
    public interface IMessenger
    {
        void SendMessage(string message);
    }
}
